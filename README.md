# README #

This README would normally document whatever steps are necessary to get your application up and running.

## Weather App installation guidelines, Test cases and Improvement areas ##

### Prerequisites to run Weather App ###

* Xcode (Simulator)
* iPhone 5s - iPhone X (Optional)
* Internet Connection

### App installation and running guidelines ###
* Download application from https://satyadip@bitbucket.org/satyadip/weather.git
* Open WeatherApplication.xcode project from Xcode and Run on simulator (iPhone SE to iPhone X). To run on Real device valid provisioning profile is required and device needs to be registered.


###  Feature Test Cases to verify ###

This application displays 5 days weather reports of any location. Following feature test  cases are required to verify application is running fine. 

* Open WeatherApp to find upcoming 5 days weather report of Default location (Kolkata). 
* Click on search icon and input any other valid location (London). Click on search icon on Keyboard and it will give upcoming 5 days weather of entered city. 
* Click on search bar and input any invalid city (“londonnnnnn”). Click on search icon on Keyboard. Message will show “City Not found”
* Disable internet connection and test the Application. It will display “Network is not available”
* Enable internet connection and test the Application. It will automatically refresh the page and it will display updated weather report of displaying city. 
* Swipe the page down to view updated weather report of displayed city.

###  Improvement Areas ###

* Focus on testing of the developed application.
* The users of the application should be given the choice to view temperature in Celsius or Fahrenheit.
* User should be given choice to view weather report for the number of days given as input by them.
* Writing unit and UI test cases for the application to feel more confident in deploying the code and to lead less bugs in the application.
* Data caching technology should have been implemented by storing previous values (3/4 times a day) in the database so that the user can view weather report even when there is no internet connection.
* Making code more modular by following MVVM design pattern for reusability, improved testability, transparent communication

