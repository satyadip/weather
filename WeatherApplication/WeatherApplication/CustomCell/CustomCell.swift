
import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgWeather: UIImageView!
    @IBOutlet weak var lblDegree: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblMaxTemp: UILabel!
    @IBOutlet weak var lblMinTemp: UILabel!
    @IBOutlet weak var lblWind: UILabel!
    @IBOutlet weak var lblWeatherDesc: UILabel!
    
    
    let imageCache = NSCache<NSString, UIImage>()
    var imageDwn: UIImage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func bindCellData(data : [String: Any]) {
        
        let baseIconPath = "http://openweathermap.org/img/w/"

        if let dictWeather = data["weather"] as? [[String: Any]] {
            if let description = dictWeather[0]["description"] as? String {
                lblWeatherDesc.text = description
            }
            if let iconString = dictWeather[0]["icon"] as? String {
                let iconFullPath = baseIconPath + iconString + ".png"
                self.imageFromServerURL(iconFullPath, withCellImage: self.imgWeather)
            }
        }
        if let dicTemp = data["temp"] as? NSDictionary {
            if let maxVal = dicTemp["max"] {
                lblMaxTemp.text = "Max Temp: " + (maxVal as! NSNumber).stringValue
            }
            if let minVal = dicTemp["min"] {
                lblMinTemp.text = "Min Temp: " + (minVal as! NSNumber).stringValue
            }
        }
        if let tempVal = data["deg"] {
            let celciusValue = ((((tempVal as! NSNumber).floatValue - 32 ) * 5) / 9)
            lblDegree.text = String(format: "%.1f˚C", celciusValue)
        }
        if let windVal = data["speed"] {
            let windValue = (windVal as! NSNumber).floatValue
            lblWind.text = String(format: "Speed : %.2f m/s", windValue)
        }
        if  let  timeResult = (data["dt"] as? Double) {
            lblTime.text = calculateDateTime(timeResult: timeResult)
        }
        if let humidity = data["humidity"] {
            lblHumidity.text = "Humidity: " + (humidity as! NSNumber).stringValue + "%"
        }
    }
    
    func calculateDateTime(timeResult: Double) -> String {
        let date = Date(timeIntervalSince1970: timeResult)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateStyle = DateFormatter.Style.short
        let localDate = dateFormatter.string(from: date)
        let fullNameArr = localDate.components(separatedBy: ", ")
        let strDateTime = fullNameArr[0] + "\n" + fullNameArr[1]
        return strDateTime
    }
    
    func imageFromServerURL(_ URLString: String ,withCellImage : UIImageView) {
        self.imageDwn = nil
        if let cachedImage = imageCache.object(forKey: NSString(string: URLString)) {
            self.imageDwn = cachedImage
            return
        }
        if let url = URL(string: URLString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            self.imageCache.setObject(downloadedImage, forKey: NSString(string: URLString))
                            withCellImage.image = downloadedImage
                        }
                    }
                }
            }).resume()
        }
    }
}

