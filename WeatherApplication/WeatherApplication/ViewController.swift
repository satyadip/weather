
import UIKit

class ViewController: UIViewController {
    
    let cellReuseIdentifier = "cell"
    var appID = "a4ca82b3480fa9e815abdf9dae96d722"
    var count = 5
    var arrItems : [[String:Any]] = []
    var callHandler : APIHandlerProtocol?
    var isNetworkAvailable : Bool = false
    let networkManager = NetworkManager()
    var JSONData :  [String : Any] = [:]
    var baseURLString : String = "http://api.openweathermap.org/data/2.5/forecast/daily?type=accurate&mode=json&units=metric&"
    
    
    @IBOutlet weak var btnsearch: NSLayoutConstraint!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var search_WD_const: NSLayoutConstraint!
    @IBOutlet weak var tblResult: UITableView!
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.black
        refreshControl.backgroundColor = UIColor.clear
        return refreshControl
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        search_WD_const.constant = 0;
        let notificationCenter = NotificationCenter.default
        
        notificationCenter.addObserver(self,
                                       selector: #selector(networkUnReachable),
                                       name: NetworkUnreachableNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(networkReachable),
                                       name: NetworkReachableNotification,
                                       object: nil)
        
        callHandler = APIHandler() as APIHandlerProtocol
        
        baseURLString = baseURLString + "&appid=" + appID + "&cnt=" + String(count)
        tblResult.addSubview(self.refreshControl)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func networkUnReachable() {
        isNetworkAvailable = false
        self.view.makeToast(message: "Network is not available", duration: 3, position: HRToastPositionDefault as AnyObject)
    }

    @objc func networkReachable() {
        
        isNetworkAvailable = true
        serviceCall()
        
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        serviceCall()
        self.refreshControl.endRefreshing()
    }
    
    @IBAction func btnSearchPressed(_ sender: Any) {
        self.search_WD_const.constant = 183;
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        lblCityName.isHidden = true
    }
    
    func serviceCall() {
        
        guard isNetworkAvailable else {
            self.view.makeToast(message: "Network is not available", duration: 3, position: HRToastPositionDefault as AnyObject)
            return
        }
        
        guard (lblCityName.text?.count)! > 0 else {
            self.arrItems = []
            self.view.makeToast(message: "City not found", duration: 0.6, position: HRToastPositionDefault as AnyObject)
            self.tblResult.reloadData()
            return
        }
        
        let urlString = baseURLString + "&q=" + lblCityName.text!
        showBlurLoader()
        callHandler?.callService(urlString: urlString,complitionHandler: { [weak  self] result in
            
            weak var wVc = self
            print(result)
            if result.count > 0 {
                wVc!.JSONData = result as! [String : Any]
                wVc?.updteRecord()
                
            }
            else {
                wVc!.view.makeToast(message: "Something went wrong", duration: 0.6, position: HRToastPositionDefault as AnyObject)
                wVc?.removeBluerLoader()
                return
            }
            
        })
        
    }
    
    func updteRecord() {
        DispatchQueue.main.async {
            if let array = self.JSONData["list"] {
                self.arrItems = (array as? [[String:Any]])!
            }
            else {
                self.arrItems = []
                self.view.makeToast(message: "City not found", duration: 0.6, position: HRToastPositionDefault as AnyObject)
                self.view.hideToastActivity()
            }
            self.tblResult?.reloadData()
            self.removeBluerLoader()
            
        }
        
    }
    
    func showBlurLoader(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.startAnimating()
        
        blurEffectView.contentView.addSubview(activityIndicator)
        activityIndicator.center = blurEffectView.contentView.center
        
        self.view.addSubview(blurEffectView)
    }
    
    func removeBluerLoader(){
        self.view.subviews.compactMap {  $0 as? UIVisualEffectView }.forEach {
            $0.removeFromSuperview()
        }
    }
    
    
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CustomCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CustomCell
        
        let items = arrItems[indexPath.row]
        cell.bindCellData(data: items)
        cell.selectionStyle = .none
        cell.layoutIfNeeded()
        return cell
    }
    
}

extension ViewController : UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtSearch.resignFirstResponder()
        lblCityName.isHidden = false
        lblCityName.text = txtSearch.text
        self.search_WD_const.constant = 0;
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        serviceCall()
        return true
    }
}

