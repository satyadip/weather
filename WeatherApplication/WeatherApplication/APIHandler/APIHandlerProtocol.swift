
import UIKit

protocol APIHandlerProtocol {
    func callService(urlString: String, complitionHandler: @escaping ((NSDictionary)->Void))
}

