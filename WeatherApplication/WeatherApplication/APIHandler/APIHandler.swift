
import UIKit

class APIHandler: APIHandlerProtocol {

    
    func callService(urlString: String, complitionHandler: @escaping ((NSDictionary)->Void))  {
        
        let url = URL(string: urlString)
        let errDict = [:] as NSDictionary
        
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
                complitionHandler(errDict)
            }else{
                do{
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                        complitionHandler((jsonResult as NSDictionary))
                        print(jsonResult)
                    }
                }
                catch{
                    complitionHandler(errDict)
                    print("Somthing wrong")
                }
            }
        }).resume()

    }
}
